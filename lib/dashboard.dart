import 'package:flutter/material.dart';
import 'package:assessment/profile.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Dashboard'),
          elevation: .1,
          backgroundColor: Colors.indigo,
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
          child: GridView.count(
            crossAxisCount: 2,
            padding: const EdgeInsets.all(3.0),
            children: <Widget>[
              makeDashboardItem("Info 1", Icons.book),
              makeDashboardItem("Info 2", Icons.book),
              ElevatedButton.icon(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ProfilePage()));
                },
                icon: const Icon(Icons.settings, size: 25.0),
                label: const Text("Edit Profile"),
              )
            ],
          ),
        ));
  }
}

Card makeDashboardItem(String title, IconData icon) {
  return Card(
    elevation: 1.0,
    margin: const EdgeInsets.all(8.0),
    child: Container(
        decoration: const BoxDecoration(color: Colors.blue),
        child: InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                const SizedBox(height: 10.0, width: 10.0),
                Center(
                    child: Icon(
                  icon,
                  size: 25.0,
                  color: Colors.white,
                )),
                const SizedBox(height: 10.0, width: 10.0),
                Center(
                  child: Text(title,
                      style:
                          const TextStyle(fontSize: 18.0, color: Colors.black)),
                )
              ],
            ))),
  );
}
